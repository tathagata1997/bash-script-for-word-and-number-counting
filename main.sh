#!/bin/bash

# Navigate to the exercise folder
input_file="feature122/input.txt"

# Count
total_count=$(cat input.txt | grep -o -E '\b[0-9]*\.[0-9]*\b|\b[0-9]+\b|\b\w+\b' | wc -w)

# Output
echo "$total_count"
