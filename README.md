# Bash Script for Word and Number Counting



## Getting started

This repository contains my solution for Exercise 122 of DevOps Course, a bash script developed for Ubuntu 22.04. The script, named main.sh, is designed to count the words and numbers in an input file and produce the output. The  input.txt script processes the input file as described in the task and produces the output in output.txt. 



The program is invoked using the provided procedure:


### Clone the repository:

```
git clone git@gitlab.com:tathagata1997/bash-script-for-word-and-number-counting.git
```
### Make the script executable:

```
chmod +x main.sh
```
### Make the script executable:

```
bash ./main.sh < input.txt > output.txt
```

```
git remote add origin https://gitlab.com/tathagata1997/bash-script-for-word-and-number-counting.git
git branch -M main
git push -uf origin main
```
### (Optional) View the output:

```
cat output.txt
```
